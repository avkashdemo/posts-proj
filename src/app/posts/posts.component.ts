import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { interval } from 'rxjs';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  posts$: Object;
  post: Object;
  tempPost$: [];
  searchKey: string = "";
  isLoadingResults:boolean = false;
  showPopup:boolean = false;
  
  constructor(private data: DataService) { }

  ngOnInit() {
    this.getRecords();
    const source = interval(10000);
    source.subscribe(val =>  this.getRecords());
  }

  getRecords() {
    this.isLoadingResults = true;
    this.data.getPosts().subscribe(
      data => { 
        this.tempPost$ = data.hits;
        this.filterPosts(this.searchKey);
        this.isLoadingResults = false;
      } 
    );
  }

  filterPosts(searchValue : string ) {  
    this.searchKey = searchValue;
    this.posts$ = this.tempPost$.filter((item:any) => item.title.toLowerCase().indexOf(searchValue.toLowerCase())!= -1)
  }

  showDetail(post) {
    this.post = post;
    this.showPopup = true;
  }

  disposeModal() {
    this.post = {};
    this.showPopup = false;
  }
}
